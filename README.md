# speedtest

Speedtest provides a Crystal interface for the Speedtest CLI.

**NOTE:** The installation of the Speedtest CLI can be complicated on some OSes, so as this release, it must be installed manually separately to the installation of this shard.

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     speedtest:
       github: HCLarsen/speedtest
   ```

2. Run `shards install`

## Usage

```crystal
require "speedtest"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/HCLarsen/speedtest/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Chris Larsen](https://gitlab.com/HCLarsen) - creator and maintainer
